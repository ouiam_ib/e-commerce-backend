package org.example.ecommercebackend.Dao;

import org.example.ecommercebackend.Entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

//accept calls from web browser scripts for this origin
//origin is protocol+ hostname+ port
@CrossOrigin("http://localhost:4200/") // adding an origin
@RepositoryRestResource(collectionResourceRel = "Product", path = "products" )
public interface ProductRepository extends JpaRepository<Product, Long> {
}
